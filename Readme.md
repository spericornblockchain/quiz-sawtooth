DESCRIPTION ABOUT THE DApp:

The Quiz Management Decentralised Application(DApp) on Hyperledger Sawtooth Platform is a Quiz management system where the University, Professors, and Students have access. The University client can register the Professors and Students to the University, the Professor client can create quizzes with multiple choice answers to a particular batch, and the Student client can attend the quizzes assigned to them and view their scorecard.



INSTRUCTIONS FOR SETTING-UP APPLICATION Requirements:

- [X] OS: Ubuntu 18.04 (Recommented)
- [X] NodeJs version 10.0 stable npm latest
- [X] Docker




STEPS:

1. Clone & Navigate into main directory
2. Run "sudo docker-compose up"
3. Open Browser & navigate LocalHost- http://localhost:4200 or Ip Address- http://127.0.0.1:4200
